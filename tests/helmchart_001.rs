use helmtester::kinds::Kind;
use helmtester::queries::Queries;
use helmtester::yaml;
use helmtester::HelmRunner;

const HELMCHART_001: &str = "tests/data/helmchart_001";

#[test]
fn feature_flag_add_enabled() -> Result<(), Box<dyn std::error::Error>> {
    let documents = HelmRunner::run(
        HELMCHART_001,
        &[yaml! {"
        ---
        app:
          features:
            add: true
        "}],
    )?;
    let config_maps = documents.by_kind(Kind::ConfigMap);
    let config_map = config_maps.first().unwrap();
    assert_eq!(config_map["data"]["FEAT_ADD"].as_str().unwrap(), "true");
    Ok(())
}

#[test]
fn feature_flag_sub_enabled() -> Result<(), Box<dyn std::error::Error>> {
    let documents = HelmRunner::run(
        HELMCHART_001,
        &[yaml! {"
        ---
        app:
          features:
            sub: true
        "}],
    )?;
    let config_maps = documents.by_kind(Kind::ConfigMap);
    let config_map = config_maps.first().unwrap();
    assert_eq!(config_map["data"]["FEAT_SUB"].as_str().unwrap(), "true");
    Ok(())
}

#[test]
fn feature_flag_mul_enabled() -> Result<(), Box<dyn std::error::Error>> {
    let documents = HelmRunner::run(
        HELMCHART_001,
        &[yaml! {"
        ---
        app:
          features:
            mul: true
        "}],
    )?;
    let config_maps = documents.by_kind(Kind::ConfigMap);
    let config_map = config_maps.first().unwrap();
    assert_eq!(config_map["data"]["FEAT_MUL"].as_str().unwrap(), "true");
    Ok(())
}

#[test]
fn feature_flag_div_enabled() -> Result<(), Box<dyn std::error::Error>> {
    let documents = HelmRunner::run(
        HELMCHART_001,
        &[yaml! {"
        ---
        app:
          features:
            div: true
        "}],
    )?;
    let config_maps = documents.by_kind(Kind::ConfigMap);
    let config_map = config_maps.first().unwrap();
    assert_eq!(config_map["data"]["FEAT_DIV"].as_str().unwrap(), "true");
    Ok(())
}

#[test]
fn feature_flags_disabled() -> Result<(), Box<dyn std::error::Error>> {
    let documents = HelmRunner::run(HELMCHART_001, &[])?;
    let config_maps = documents.by_kind(Kind::ConfigMap);
    let config_map = config_maps.first().unwrap();
    assert_eq!(config_map["data"]["FEAT_ADD"].as_str().unwrap(), "false");
    assert_eq!(config_map["data"]["FEAT_SUB"].as_str().unwrap(), "false");
    assert_eq!(config_map["data"]["FEAT_MUL"].as_str().unwrap(), "false");
    assert_eq!(config_map["data"]["FEAT_DIV"].as_str().unwrap(), "false");
    Ok(())
}

#[test]
#[rustfmt::skip]
fn resource_setting_cpu() -> Result<(), Box<dyn std::error::Error>> {
    let documents = HelmRunner::run(
        HELMCHART_001,
        &[yaml! {"
        ---
        deployment:
          resources:
            cpu: 500m
        "}],
    )?;
    let deployments = documents.by_kind(Kind::Deployment);
    let deployment = deployments.first().unwrap();
    assert_eq!(deployment["spec"]["template"]["spec"]["containers"][0]["resources"]["limits"]["cpu"].as_str().unwrap(), "500m");
    assert_eq!(deployment["spec"]["template"]["spec"]["containers"][0]["resources"]["requests"]["cpu"].as_str().unwrap(), "500m");
    Ok(())
}

#[test]
#[rustfmt::skip]
fn resource_setting_memory() -> Result<(), Box<dyn std::error::Error>> {
    let documents = HelmRunner::run(
        HELMCHART_001,
        &[yaml! {"
        ---
        deployment:
          resources:
            memory: 512Mi
        "}],
    )?;
    let deployments = documents.by_kind(Kind::Deployment);
    let deployment = deployments.first().unwrap();
    assert_eq!(deployment["spec"]["template"]["spec"]["containers"][0]["resources"]["limits"]["memory"].as_str().unwrap(), "512Mi");
    assert_eq!(deployment["spec"]["template"]["spec"]["containers"][0]["resources"]["requests"]["memory"].as_str().unwrap(), "512Mi");
    Ok(())
}

#[test]
fn filter_query_works() -> Result<(), Box<dyn std::error::Error>> {
    let documents = HelmRunner::run(HELMCHART_001, &[])?;
    let deployments = documents.by_kind(Kind::Deployment);
    assert!(!deployments.is_empty());
    Ok(())
}
