use std::io::Write;
use std::path::PathBuf;

#[derive(Debug)]
pub struct TempFile {
    content: String,
    location: PathBuf,
}

impl TempFile {
    pub fn new(content: &str) -> Self {
        let mut path = PathBuf::new();
        path.push(std::env::temp_dir());
        path.push(format!(
            "{}.{}.{}",
            "helm-values-test",
            uuid::Uuid::new_v4(),
            "yaml"
        ));
        TempFile {
            content: content.to_owned(),
            location: path,
        }
    }

    pub fn path_as_string(&self) -> String {
        self.location.to_string_lossy().to_string()
    }

    pub fn create_file(&self) -> Result<(), std::io::Error> {
        let mut file = std::fs::File::create(self.location.as_path())?;
        file.write_all(self.content.as_bytes())?;
        Ok(())
    }

    pub fn delete_file(&self) -> Result<(), std::io::Error> {
        let path = self.location.as_path();
        if std::fs::metadata(path).is_ok() {
            std::fs::remove_file(path)?;
        }
        Ok(())
    }
}
