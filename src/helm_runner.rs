use std::path::PathBuf;
use std::process::Command;

use scopeguard::defer;
use thiserror::Error;

use crate::tempfile::TempFile;

#[derive(Debug, Error)]
pub enum HelmRunnerError {
    #[error("`helm` was not found in $PATH")]
    HelmNotInstalled,

    #[error("`helm` exited abnormally: {stderr}")]
    HelmExitError { stderr: String },

    #[error("IoError: `{0}`")]
    IoError(#[from] std::io::Error),

    #[error("SerdeYamlError: `{0}`")]
    SerdeYamlError(#[from] serde_yaml::Error),
}

#[derive(Debug)]
pub struct HelmRunner {
    path: PathBuf,
    config_files: Vec<TempFile>,
    documents: Vec<serde_yaml::Value>,
}

impl HelmRunner {
    pub fn new(path: &str) -> Self {
        HelmRunner {
            path: path.into(),
            config_files: vec![],
            documents: vec![],
        }
    }

    pub fn execute(&mut self) -> Result<&[serde_yaml::Value], HelmRunnerError> {
        if !command_exists("helm") {
            return Err(HelmRunnerError::HelmNotInstalled);
        }
        let stdout = self.run_helm_command()?;
        let mut yaml_documents = vec![];
        let yaml_document_strings: Vec<&str> = stdout
            .split("---\n")
            .filter(|chunk| !chunk.trim().is_empty())
            .collect();
        for yaml in yaml_document_strings {
            yaml_documents.push(serde_yaml::from_str(yaml)?);
        }
        self.documents = yaml_documents;
        Ok(self.documents.as_ref())
    }

    fn run_helm_command(&self) -> Result<String, HelmRunnerError> {
        defer!({
            if let Err(err) = self.delete_all_temp_files() {
                eprintln!("WARNING: HelmRunner.run(): failed to delete temp files: {err}");
            }
        });
        self.create_all_temp_files()?;
        let mut cmd = Command::new("helm");
        cmd.args(["template", self.get_absolute_chart_path()?.as_str()]);
        for file in self.config_files.iter() {
            cmd.args(["-f", file.path_as_string().as_ref()]);
        }
        let output = cmd.output()?;
        if !output.status.success() {
            return Err(HelmRunnerError::HelmExitError {
                stderr: String::from_utf8_lossy(&output.stderr).to_string(),
            });
        }
        Ok(String::from_utf8_lossy(&output.stdout).to_string())
    }

    pub fn add_values_yaml(&mut self, content: &str) -> Result<(), HelmRunnerError> {
        // to check if the content is valid yaml
        let _ = serde_yaml::from_str::<serde_yaml::Value>(content)?;
        self.config_files.push(TempFile::new(content));
        Ok(())
    }

    fn create_all_temp_files(&self) -> Result<(), HelmRunnerError> {
        for file in self.config_files.iter() {
            file.create_file()?;
        }
        Ok(())
    }

    fn delete_all_temp_files(&self) -> Result<(), HelmRunnerError> {
        for file in self.config_files.iter() {
            file.delete_file()?;
        }
        Ok(())
    }

    fn get_absolute_chart_path(&self) -> Result<String, HelmRunnerError> {
        Ok(std::fs::canonicalize(&self.path)?
            .to_string_lossy()
            .to_string())
    }

    pub fn dump(&self) -> Result<(), HelmRunnerError> {
        for doc in self.documents.iter() {
            println!("{}", serde_yaml::to_string(doc)?);
        }
        Ok(())
    }

    pub fn run(
        path: &str,
        value_yamls: &[&str],
    ) -> Result<Vec<serde_yaml::Value>, HelmRunnerError> {
        let mut helm = HelmRunner::new(path);
        for yaml in value_yamls.iter() {
            helm.add_values_yaml(yaml)?;
        }
        let documents = helm.execute()?;
        Ok(documents.into())
    }
}

fn command_exists(cmd: &str) -> bool {
    if let Ok(paths) = std::env::var("PATH") {
        for path in paths.split(':') {
            let p_str = format!("{path}/{cmd}");
            if std::fs::metadata(p_str).is_ok() {
                return true;
            }
        }
    }
    false
}
