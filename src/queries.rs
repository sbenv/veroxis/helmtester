use serde_yaml::Value;

use crate::kinds::Kind;

pub trait Queries {
    fn by_kind(&self, kind: Kind) -> Vec<&Value>;
}

impl Queries for Vec<Value> {
    fn by_kind(&self, kind: Kind) -> Vec<&Value> {
        let mut documents = vec![];
        for doc in self.iter() {
            if let Some(kind_str) = doc["kind"].as_str() {
                if kind_str == Kind::as_str(kind) {
                    documents.push(doc);
                }
            }
        }
        documents
    }
}
